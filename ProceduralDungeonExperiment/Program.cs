﻿//-----------------------------------------------------------------------
// <copyright file="Program.cs" company="Eric Hargitt">
//     Copyright (c) 2018 Eric Hargitt
// </copyright>
//-----------------------------------------------------------------------

namespace ProceduralDungeonExperiment
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using ProceduralDungeonExperiment.Dungeon;

    /// <summary>
    /// Class that contains the main entry for the application.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            DungeonDisplay resultForm = new DungeonDisplay();

            DungeonParameters parameters;
            parameters.NumberOfRooms = 5;
            parameters.MinRoomSize = 5;
            parameters.MaxRoomSize = 10;
            Grid result = Dungeon.Dungeon.Generate(parameters);
            resultForm.SetDungeon(result.ToString());

            Application.Run(resultForm);
        }
    }
}
