﻿//-----------------------------------------------------------------------
// <copyright file="DungeonDisplay.cs" company="Eric Hargitt">
//     Copyright (c) 2018 Eric Hargitt
// </copyright>
//-----------------------------------------------------------------------
namespace ProceduralDungeonExperiment
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    /// <summary>
    /// A simple form to display a generated dungeon.
    /// </summary>
    public partial class DungeonDisplay : Form
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DungeonDisplay"/> class.
        /// </summary>
        public DungeonDisplay()
        {
            this.InitializeComponent();
        }
    }
}
