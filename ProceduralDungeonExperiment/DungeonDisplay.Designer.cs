﻿//-----------------------------------------------------------------------
// <copyright file="DungeonDisplay.Designer.cs" company="Eric Hargitt">
//     Copyright (c) 2018 Eric Hargitt
// </copyright>
//-----------------------------------------------------------------------

namespace ProceduralDungeonExperiment
{
    /// <summary>
    /// A simple form to display a generated dungeon.
    /// </summary>
    public partial class DungeonDisplay
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Holds the text that represents the dungeon.
        /// </summary>
        private System.Windows.Forms.TextBox dungeon;

        /// <summary>
        /// Sets the string that represents the dungeon for the form.
        /// </summary>
        /// <param name="input">The string that represents the dungeon to display in the form.</param>
        public void SetDungeon(string input)
        {
            this.dungeon.Text = input;
            this.dungeon.Refresh();
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dungeon = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // dungeon
            // 
            this.dungeon.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dungeon.BackColor = System.Drawing.Color.Black;
            this.dungeon.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dungeon.ForeColor = System.Drawing.Color.White;
            this.dungeon.Location = new System.Drawing.Point(13, 13);
            this.dungeon.Multiline = true;
            this.dungeon.Name = "dungeon";
            this.dungeon.ReadOnly = true;
            this.dungeon.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.dungeon.Size = new System.Drawing.Size(775, 436);
            this.dungeon.TabIndex = 0;
            // 
            // DungeonDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.dungeon);
            this.Name = "DungeonDisplay";
            this.Text = "Dungeon Display";
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
    }
}
