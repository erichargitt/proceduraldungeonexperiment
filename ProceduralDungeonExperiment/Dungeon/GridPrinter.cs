﻿//-----------------------------------------------------------------------
// <copyright file="GridPrinter.cs" company="Eric Hargitt">
//     Copyright (c) 2018 Eric Hargitt
// </copyright>
//-----------------------------------------------------------------------
namespace ProceduralDungeonExperiment.Dungeon
{
    using System;

    /// <summary>
    /// Methods to print out a Grid for visual inspection.
    /// </summary>
    public static class GridPrinter
    {
        /// <summary>
        /// Prints a Grid object to the console.
        /// </summary>
        /// <param name="input">The Grid to print.</param>
        public static void ToConsole(Grid input)
        {
            Console.Write(input.ToString());
        }
    }
}
