﻿//-----------------------------------------------------------------------
// <copyright file="Room.cs" company="Eric Hargitt">
//     Copyright (c) 2018 Eric Hargitt
// </copyright>
//-----------------------------------------------------------------------
namespace ProceduralDungeonExperiment.Dungeon
{
    using System;

    /// <summary>
    /// Represents a coordinate-aware, rectangular 2D room on a grid.
    /// </summary>
    public class Room
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Room"/> class.
        /// </summary>
        /// <param name="sizeX">The number of spaces the Room spans on the x axis.</param>
        /// <param name="sizeY">The number of spaces the Room spans on the y axis.</param>
        public Room(int sizeX, int sizeY)
        {
            this.SizeX = sizeX;
            this.SizeY = sizeY;
            this.Id = Guid.NewGuid();
        }

        /// <summary>
        /// Gets the location of the upper-left corner space of the Room on the x axis.
        /// </summary>
        public int UpperLeftX { get; private set; }

        /// <summary>
        /// Gets the location of the upper-left corner space of the Room on the y axis.
        /// </summary>
        public int UpperLeftY { get; private set; }

        /// <summary>
        /// Gets the number of spaces the Room spans on the x axis.
        /// </summary>
        public int SizeX { get; private set; }

        /// <summary>
        /// Gets the number of spaces the Room spans on the y axis.
        /// </summary>
        public int SizeY { get; private set; }

        /// <summary>
        /// Gets the GUID for the Room.
        /// </summary>
        public Guid Id { get; private set; }
    }
}
