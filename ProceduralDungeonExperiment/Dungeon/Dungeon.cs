﻿//-----------------------------------------------------------------------
// <copyright file="Dungeon.cs" company="Eric Hargitt">
//     Copyright (c) 2018 Eric Hargitt
// </copyright>
//-----------------------------------------------------------------------

namespace ProceduralDungeonExperiment.Dungeon
{
    using System;

    /// <summary>
    /// Contains utilities to generate 2D dungeons on a grid.
    /// </summary>
    public static class Dungeon
    {
        /// <summary>
        /// Generate a 2D dungeon with the specified parameters.
        /// </summary>
        /// <param name="input">The specifications for the dungeon that will be generated.</param>
        /// <returns>A <see cref="Grid"/> object that represents the dungeon's layout</returns>
        public static Grid Generate(DungeonParameters input)
        {
            // Determine Grid size
            int gridSize = (input.MaxRoomSize * input.NumberOfRooms) * 2;
            Grid returnGrid = new Grid(gridSize, gridSize, false);

            // Determine max coordinates as to not go off the Grid when placing a room
            int maxCoord = gridSize - input.MaxRoomSize;

            // Set up RNG
            Random rnd = new Random();

            for (int i = 0; i < input.NumberOfRooms; ++i)
            {
                // Make the room
                int newSizeX = rnd.Next(input.MinRoomSize, input.MaxRoomSize + 1);
                int newSizeY = rnd.Next(input.MinRoomSize, input.MaxRoomSize + 1);
                Room r = new Room(newSizeX, newSizeY);

                // Place the room
                int newCoordX = 0;
                int newCoordY = 0;
                bool foundFit = false;
                do
                {
                    newCoordX = rnd.Next(0, maxCoord + 1);
                    newCoordY = rnd.Next(0, maxCoord + 1);
                    foundFit = returnGrid.IsVacant(newCoordX, newCoordY, newSizeX, newSizeY);
                }
                while (foundFit == false);

                returnGrid.Occupy(newCoordX, newCoordY, newSizeX, newSizeY);
            }

            return returnGrid;
        }
    }
}
