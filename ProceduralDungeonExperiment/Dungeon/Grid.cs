﻿//-----------------------------------------------------------------------
// <copyright file="Grid.cs" company="Eric Hargitt">
//     Copyright (c) 2018 Eric Hargitt
// </copyright>
//-----------------------------------------------------------------------
namespace ProceduralDungeonExperiment.Dungeon
{
    using System;
    using System.Text;

    /// <summary>
    /// A class that represents a grid of spaces. Spaces can be occupied or unoccupied. The space at x = 0 and y = 0 is the upper-left corner of the Grid.
    /// </summary>
    public class Grid
    {
        /// <summary>
        /// Holds the internal state of the Grid. Elements set to true if space is occupied.
        /// </summary>
        private bool[,] internalGrid;

        /// <summary>
        /// Initializes a new instance of the <see cref="Grid"/> class. The Grid's spaces are either fully occupied or fully unoccupied.
        /// </summary>
        /// <param name="xSize">The length of the grid (x axis).</param>
        /// <param name="ySize">The height of the grid (y axis).</param>
        /// <param name="initAsOccupied">Fully occupy all grid spaces if true.</param>
        public Grid(int xSize, int ySize, bool initAsOccupied = true)
        {
            this.internalGrid = new bool[xSize, ySize];

            if (initAsOccupied)
            {
                for (int x = 0; x < xSize; ++x)
                {
                    for (int y = 0; y < ySize; ++y)
                    {
                        this.internalGrid[x, y] = initAsOccupied;
                    }
                }
            }
        }
 
        /// <summary>
        /// Gets the length of the grid (x axis).
        /// </summary>
        public int SizeX
        {
            get
            {
                return this.internalGrid.GetLength(0);
            }
        }

        /// <summary>
        /// Gets the height of the grid (y axis).
        /// </summary>
        public int SizeY
        {
            get
            {
                return this.internalGrid.GetLength(1);
            }
        }

        /// <summary>
        /// Gets/sets the occupation of the Grid space at the given coordinates.
        /// </summary>
        /// <param name="x">The x axis coordinate.</param>
        /// <param name="y">The y axis coordinate.</param>
        /// <returns>True if Grid space is occupied and false otherwise.</returns>
        public bool this[int x, int y]
        {
            get
            {
                try
                {
                    return this.internalGrid[x, y];
                }
                catch (System.IndexOutOfRangeException)
                {
                    throw this.BadGridIndex(x, y);
                }
            }

            set
            {
                try
                {
                    this.internalGrid[x, y] = value;
                }
                catch
                {
                    throw this.BadGridIndex(x, y);
                }
            }
        }

        /// <summary>
        /// Determine if there is a vacant space for the specified rectangle at the specified coordinate.
        /// </summary>
        /// <param name="x">The x coordinate of the requested rectangle's upper-left corner.</param>
        /// <param name="y">The y coordinate of the requested rectangle's upper-left corner.</param>
        /// <param name="sizeX">The size of the requested rectangle on the x axis.</param>
        /// <param name="sizeY">The size of the requested rectangle on the y axis.</param>
        /// <returns>True if there is a vacancy for the specified rectangle at the specified location, and false otherwise.</returns>
        public bool IsVacant(int x, int y, int sizeX, int sizeY)
        {
            for (int i = x; i < x + sizeX; ++i)
            {
                for (int j = y; j < y + sizeY; ++j)
                {
                    if (this[i, j] == true)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Occupy the Grid with the specified rectangle.
        /// </summary>
        /// <param name="x">The x coordinate of the requested rectangle's upper-left corner.</param>
        /// <param name="y">The y coordinate of the requested rectangle's upper-left corner.</param>
        /// <param name="sizeX">The size of the requested rectangle on the x axis.</param>
        /// <param name="sizeY">The size of the requested rectangle on the y axis.</param>
        public void Occupy(int x, int y, int sizeX, int sizeY)
        {
            if (this.IsVacant(x, y, sizeX, sizeY) == false)
            {
                throw new InvalidOperationException("Space on Grid is already occupied. Verify space is empty using IsVacant()");
            }

            for (int i = x; i < x + sizeX; ++i)
            {
                for (int j = y; j < y + sizeY; ++j)
                {
                    this[i, j] = true;
                }
            }
        }

        /// <summary>
        /// Returns a string version of the grid.
        /// </summary>
        /// <returns>The string representation of the grid.</returns>
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < this.SizeY; ++i)
            {
                for (int j = 0; j < this.SizeX; ++j)
                {
                    if (this[j, i])
                    {
                        builder.Append('#');
                    }
                    else
                    {
                        builder.Append('O');
                    }
                }

                builder.Append("\r\n");
            }

            return builder.ToString();
        }

        /// <summary>
        /// Returns a formatted exceptions indicating which index is out of bounds
        /// </summary>
        /// <param name="x">The x axis coordinate.</param>
        /// <param name="y">The y axis coordinate.</param>
        /// <returns>The formatted exception.</returns>
        private System.ArgumentException BadGridIndex(int x, int y)
        {
            string badIndicies = string.Empty;
            if (x < 0 || x >= this.SizeX)
            {
                badIndicies += "[x]";
            }

            if (y < 0 || y >= this.SizeY)
            {
                badIndicies += "[y]";
            }

            System.ArgumentException argEx = new System.ArgumentException("1 or both Grid indicies are out of range", "index");
            return argEx;
        }
    }
}
