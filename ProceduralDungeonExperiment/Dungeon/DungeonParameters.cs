﻿//-----------------------------------------------------------------------
// <copyright file="DungeonParameters.cs" company="Eric Hargitt">
//     Copyright (c) 2018 Eric Hargitt
// </copyright>
//-----------------------------------------------------------------------

namespace ProceduralDungeonExperiment.Dungeon
{
    /// <summary>
    /// Input parameters for generating a dungeon.
    /// </summary>
    public struct DungeonParameters
    {
        /// <summary>
        /// Gets or sets the number of main rooms the dungeon contains;
        /// </summary>
        public int NumberOfRooms;

        /// <summary>
        /// Gets or sets the minimum size of rooms in the dungeon.
        /// </summary>
        public int MinRoomSize;

        /// <summary>
        /// Gets or sets the maximum size of rooms in the dungeon.
        /// </summary>
        public int MaxRoomSize;
    }
}
