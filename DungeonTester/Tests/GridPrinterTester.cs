﻿//-----------------------------------------------------------------------
// <copyright file="GridPrinterTester.cs" company="Eric Hargitt">
//     Copyright (c) 2018 Eric Hargitt
// </copyright>
//-----------------------------------------------------------------------
namespace DungeonTester.Tests
{
    using System;
    using System.IO;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using ProceduralDungeonExperiment.Dungeon;

    /// <summary>
    /// Tests for the <see cref="GridPrinterTester"/> class.
    /// </summary>
    [TestClass]
    public class GridPrinterTester
    {
        /// <summary>
        /// Tests printing the Grid to the console.
        /// </summary>
        [TestMethod]
        public void ToConsole()
        {
            // ARRANGE
            int x = 5;
            int y = 6;
            Grid input = new Grid(x, y, false);
            input[2, 1] = true;
            input[3, 1] = true;
            input[2, 2] = true;
            input[3, 2] = true;

            StringWriter dummyConsole = new StringWriter();
            Console.SetOut(dummyConsole);
            string expectedOutput = "OOOOO\r\nOO##O\r\nOO##O\r\nOOOOO\r\nOOOOO\r\nOOOOO\r\n";

            // ACT
            GridPrinter.ToConsole(input);

            // ASSERT
            Assert.AreEqual(expectedOutput, dummyConsole.ToString());
        }
    }
}
