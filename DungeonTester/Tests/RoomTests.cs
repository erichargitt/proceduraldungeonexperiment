﻿//-----------------------------------------------------------------------
// <copyright file="RoomTests.cs" company="Eric Hargitt">
//     Copyright (c) 2018 Eric Hargitt
// </copyright>
//-----------------------------------------------------------------------
namespace DungeonTester.Tests
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using ProceduralDungeonExperiment.Dungeon;

    /// <summary>
    /// Tests for the <see cref="Room"/> class.
    /// </summary>
    [TestClass]
    public class RoomTests
    {
        /// <summary>
        /// Test that a created Room is the correct size. Also tests size getters.
        /// </summary>
        [TestMethod]
        public void CreateRoom()
        {
            // ARRANGE
            int sizeX = 42;
            int sizeY = 1337;
            Room target;

            // ACT
            target = new Room(sizeX, sizeY);

            // ASSERT
            Assert.AreEqual(sizeX, target.SizeX);
            Assert.AreEqual(sizeY, target.SizeY);
        }
    }
}
