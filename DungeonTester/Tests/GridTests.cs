﻿//-----------------------------------------------------------------------
// <copyright file="GridTests.cs" company="Eric Hargitt">
//     Copyright (c) 2018 Eric Hargitt
// </copyright>
//-----------------------------------------------------------------------
namespace DungeonTester.Tests
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using ProceduralDungeonExperiment.Dungeon;

    /// <summary>
    /// Tests for the <see cref="Grid"/> class.
    /// </summary>
    [TestClass]
    public class GridTests
    {
        /// <summary>
        /// Tests to see if the Grid will construct will all spaces fully occupied.
        /// </summary>
        [TestMethod]
        public void ConstructFullyOccupied()
        {
            // ARRANGE
            int x = 5;
            int y = 5;

            // ACT
            Grid target = new Grid(x, y);

            // ASSERT
            for (int i = 0; i < target.SizeX; ++i)
            {
                for (int j = 0; j < target.SizeY; ++j)
                {
                    Assert.IsTrue(target[i, j]);
                }
            }
        }

        /// <summary>
        /// Tests to see if isVacant works before and after manually reserving a space.
        /// </summary>
        [TestMethod]
        public void IsVacantNice()
        {
            // ARRANGE
            int x = 10;
            int y = 12;
            Grid target = new Grid(x, y, false);
            bool isVacant = false;
            bool isNotVacant = false;

            // ACT
            isVacant = target.IsVacant(1, 1, 2, 4);

            for (int i = 1; i < 3; ++i)
            {
                for (int j = 1; j < 5; ++j)
                {
                    target[i, j] = true;
                }
            }

            isNotVacant = target.IsVacant(2, 4, 2, 3) == false;

            // ASSERT
            Assert.IsTrue(isVacant);
            Assert.IsTrue(isNotVacant);
        }

        /// <summary>
        /// Tests to see if Occupy() will fully occupy the Grid with the specified rectangle.
        /// </summary>
        [TestMethod]
        public void OccupyNice()
        {
            // ARRANGE
            int x = 3;
            int y = 4;
            int sizeX = 10;
            int sizeY = 12;
            Grid target = new Grid(50, 50, false);

            // ACT
            target.Occupy(x, y, sizeX, sizeY);

            // ASSERT
            for (int i = x; i < x + sizeX; ++i)
            {
                for (int j = y; j < y + sizeY; ++j)
                {
                    Assert.IsTrue(target[i, j]);
                }
            }
        }
    }
}
